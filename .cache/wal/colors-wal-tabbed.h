static const char* selbgcolor   = "#020109";
static const char* selfgcolor   = "#f0ecc9";
static const char* normbgcolor  = "#9C5EAF";
static const char* normfgcolor  = "#f0ecc9";
static const char* urgbgcolor   = "#645EA8";
static const char* urgfgcolor   = "#f0ecc9";
