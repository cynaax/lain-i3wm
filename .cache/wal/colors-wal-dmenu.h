static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f0ecc9", "#020109" },
	[SchemeSel] = { "#f0ecc9", "#645EA8" },
	[SchemeOut] = { "#f0ecc9", "#51A2CA" },
};
